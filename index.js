'use strict';

const { Scraper } = require('./src/kickstarter.js');
const { Enricher } = require('./src/cms.js');
const { pushHook, getTimeYesterday } = require('./src/utils.js');

/**
 *
 * CRON to handle fetching of new projects every day
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
exports.syncer = async (event, context, callback) => {
    const { WEBHOOK_INTEGROMAT } = process.env;

    // 1. Call Kickstarter
    const scraper = new Scraper(callback);
    const projects = await scraper.getProjectsCompletedYesterday();

    console.log(`Got ${projects.length} projects`);

    const { firstHourPrevDay, lastHourPrevDay } = getTimeYesterday();

    // Sending webhook to Integromat with the received data
    await pushHook(WEBHOOK_INTEGROMAT, {
        projects,
        time_start: firstHourPrevDay,
        time_end: lastHourPrevDay
    });

    callback(null, {
        statusCode: 200
    });
};

/**
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
exports.shopifyer = async (event, context, callback) => {
    const project = event.body;
    const { API_KEY_WHATCMS } = process.env;

    console.log(project);

    const enricher = new Enricher(callback, API_KEY_WHATCMS);
    /*const shopify = projects.filter(project => enricher.getShopify(project.website));*/

    const shopify = await enricher.getShopify(project);

    console.log(`Got ${shopify} for ${project}`);

    callback(null, {
        statusCode: 200,
        body: shopify,
    });
}