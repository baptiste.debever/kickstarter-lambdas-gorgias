const request = require('request-promise-native');
const TODAY = new Date();

/**
 *
 * @param {int} time
 */
module.exports.delay = time => new Promise(resolve => setTimeout(resolve, time));

/**
 * Trigger a webhook
 *
 * @param {String} url
 * @param {Object} data
 * @param {Object} headers | optional
 */
module.exports.pushHook = async (url, data, headers = '') => {

    const options = {
        method: 'POST',
        uri: `${url}`,
        headers: { 'Content-type': 'application/json' },
        form: { payload: JSON.stringify(data) }
    };

    let results = {};
    try {
        results = await request(options);
    } catch (error) {
        console.log(error);
    }

    return results;
}

/**
 *
 * @param {String} url
 * @param {Object} headers
 */
module.exports.getApi = async (url, headers = '') => {

    const options = {
        method: 'GET',
        uri: `${url}`,
        headers,
        json: true,
        proxy: 'http://bdebever:-iKDAU4gE9fJNKimkhMWR@us.proxymesh.com:31280'
    };


    let results = {};
    try {
        results = await request(options);
    } catch (error) {
        console.log(error);
    }

    return results;
}

/**
 * Return formatted time from yesterday
 */
module.exports.getTimeYesterday = () => {
    let lastHourPrevDay = Math.floor((TODAY.setHours(0, 0, 0, 1) - 2) / 1000);
    let firstHourPrevDay = Math.floor(new Date(lastHourPrevDay*1000).setHours(0, 0, 0, 1) / 1000);

    return { firstHourPrevDay, lastHourPrevDay };
}