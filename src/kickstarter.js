const { getApi, delay, getTimeYesterday } = require('./utils.js');
const chromium = require('chrome-aws-lambda');

const ROOT = 'https://www.kickstarter.com/';
const BLACKLISTS = [
    'facebook.com', 'instagram.com', 'pinterest.com', 'twitter.com', 'linkedin.com', 'etsy.com', 'wixsite.com'
]

/**
 *
 */
class Scraper {
    constructor(callback) {
        this.callback = callback;
        this.browser;
        this.page;
    }

    get projects() {
        return this._projects || 'undefined';
    }

    async _init() {
        this.browser = await chromium.puppeteer.launch({
            args: chromium.args,
            defaultViewport: chromium.defaultViewport,
            executablePath: await chromium.executablePath,
            headless: chromium.headless,
        });

        this.page = await this.browser.newPage();

        return this.page;
    }

    /**
     * Call Kickstarter API to get the latest completed project
     */
    async getProjectsCompletedYesterday() {

        // Kickstarter use CEST
        const { firstHourPrevDay, lastHourPrevDay } = getTimeYesterday();
        let results = [];
        let page = 1;
        let moreToFetch=true;

        // We fetch up to 180 projects (each page returns 12) - to be improved later
        while (page<=15 && moreToFetch) {
            console.log(`Querying Kickstarter page ${page}`);

            const {projects} = await getApi(`${ROOT}projects/search.json?state=successful&sort=end_date&page=${page}`);
            page++;

            console.log(`Got ${projects.length} projects`);
            console.log(`Querying for ${firstHourPrevDay} to ${lastHourPrevDay}`);

            // TODO: create a map to only keep interesting properties in the object
            results = [
                ...results,
                ...projects.filter(project => (project.state_changed_at >= firstHourPrevDay && project.state_changed_at <= lastHourPrevDay))
                    .map(proj => ({
                        id: proj.id,
                        link: proj.urls.web.project,
                        photo: proj.photo.full,
                        name: proj.name,
                        blurb: proj.blurb,
                        goal: proj.goal,
                        pledged: proj.pledged,
                        currency: proj.currency,
                        slug: proj.slug,
                        country: proj.country,
                        country_displayable_name: proj.country_displayable_name,
                        deadline: proj.deadline,
                        state_changed_at: proj.state_changed_at,
                        launched_at: proj.launched_at,
                        backers_count: proj.backers_count,
                        creator_name: proj.creator.name,
                        creator_id: proj.creator.id,
                        creator_url: proj.creator.urls.web.user,
                        location: proj.location.name,
                        category: proj.category.name
                    }))
            ];

            // It seems like the API is not properly orderded by state_changed_at day
            // We add a second level of security and go up to 2d before
            if (projects[projects.length - 1].state_changed_at < (firstHourPrevDay-24*60*60))
                moreToFetch=false;

            await delay(15005+Math.random()*25);
        }

        // todo: Trigger Puppeteer to get website through a QUEUE

        this._projects = results;
        return results;
    }


    /**
     * NOT USED
     *
     * @param {Object} projects
     */
    async triggerCreatorScraping(projects) {
        await this._init();

        let filtered = [];
        for (let index = 0; index < results.length; index++) {
            const project = results[index];
            const website = await this._findWebsite(project);
            if (website) filtered = [...filtered, { ...project, website }]
        }

        await this.browser.close();
    }

    /**
     * Find the website URL from the creator page
     *
     * @param {Object} param0
     */
    async _findWebsite({ creator_url }) {
        if (!creator_url) return;

        console.log(creator_url);

        try {
            await this.page.goto(`${creator_url}/about`, {
                waitUntil: 'networkidle2'
            });
        } catch (error) {
            console.log(error);
        }

        await delay(6205 + Math.random() * 25);

        return await this.page.evaluate((blacklists) => {
            let websites = document.querySelectorAll('ul.menu-submenu li a');
            let website = [...websites].filter(site => !blacklists.some(blacklist => site.innerText.includes(blacklist)))

            return website[0] && website[0].innerText;
        }, BLACKLISTS)
    }
}

module.exports.Scraper = Scraper;