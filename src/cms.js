const { getApi } = require('./utils.js');

const API_ROOT = 'https://whatcms.org/APIEndpoint/Tech';

/**
 * Technology-stack API enricher
 */
class Enricher {
    constructor(callback, api_key) {
        this.callback = callback;
        this.API_KEY = api_key;
    }

    /**
     *
     * @param {String} url
     */
    async getShopify(url) {
        const {code, name, confidence, msg} = await this._getCMS(url);
        return (code === 200 && confidence === 'high' && name === 'Shopify');
    }

    /**
     *
     * @param {String} url
     */
    async _getCMS(url) {
        const { result } = await getApi(`${API_ROOT}?key=${this.API_KEY}&url=${url}`);

        console.log(result);

        return result;
    }
}

module.exports.Enricher = Enricher;